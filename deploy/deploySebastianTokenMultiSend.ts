import { ethers } from "hardhat";

async function main() {
  const [deployer] = await ethers.getSigners();
  console.log(deployer);

  console.log("Deploying contracts with the account:", deployer.address);

  const Token = await ethers.getContractFactory("SebastianTokenMultiSend");

  const token = await Token.deploy(ethers.utils.getAddress("zzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzz"));

  console.log("Token address:", token.address);
}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });